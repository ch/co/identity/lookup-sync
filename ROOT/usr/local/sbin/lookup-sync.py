#!/usr/bin/env python3

import abc
import configparser

import ibisclient
import ldap
import ldap.filter
import psycopg2
import psycopg2.extras


class LocalSourceFactory:
    def __init__(self):
        self._sources = {}

    def register(self, key, c):
        self._sources[key] = c

    def get(self, key, **kwargs):
        c = self._sources.get(key, None)
        if not c:
            raise ValueError(key)
        return c(**kwargs)


class Source(abc.ABC):
    """Base class"""

    @abc.abstractmethod
    def get_members(self):
        pass


class DatabaseSource(Source):
    def __init__(self, view_name, database_options):
        self.view_name = view_name
        self.database_options = database_options

    def get_members(self):
        # print("Connecting to Chemistry database...")

        db_conn = psycopg2.connect(**self.database_options)

        cur = db_conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        sql = f"select crsid from {self.view_name}"
        cur.execute(sql)

        db_list = []
        # Build array of CRSids for current members in DB
        for row in cur:
            db_list.append(row["crsid"])
        return db_list

    def __repr__(self):
        return f"DatabaseSource(view_name={self.view_name!r})"


class ActiveDirectoryQuery:
    base_dn = "DC=ad,DC=ch,DC=cam,DC=ac,DC=uk"
    AD_domain = "ad.ch.cam.ac.uk"
    bind_user = "ldapbind@ad.ch.cam.ac.uk"
    bind_password = "ldapbindpasswd"
    _connection = None

    @staticmethod
    def get_ldap_connection():
        ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_ALLOW)
        ldap.set_option(ldap.OPT_REFERRALS, 0)
        ldap.set_option(ldap.OPT_PROTOCOL_VERSION, ldap.VERSION3)
        ldap.set_option(ldap.OPT_NETWORK_TIMEOUT, 10.0)
        ldapuri = "ldaps://%s" % ActiveDirectoryQuery.AD_domain
        ad_conn = ldap.ldapobject.LDAPObject(ldapuri)
        ad_conn.bind_s(
            ActiveDirectoryQuery.bind_user,
            ActiveDirectoryQuery.bind_password,
            ldap.AUTH_SIMPLE,
        )
        return ad_conn

    @property
    def connection(self):
        if self._connection is None:
            self._connection = self.get_ldap_connection()
        return self._connection

    def get_group_dn(self, group_name):
        flt = "(&(objectClass=group)(cn=%s))" % (
            ldap.filter.escape_filter_chars(group_name)
        )
        try:
            res = [
                x
                for x in self.connection.search_s(
                    ActiveDirectoryQuery.base_dn, ldap.SCOPE_SUBTREE, flt
                )
                if x[0]
            ][0]
        except IndexError:
            raise Exception("NoSuchGroupError(" + group_name + ")")
        return res[0]

    def lookup_username_from_dn(self, dn):
        r = self.connection.search_s(
            dn, ldap.SCOPE_BASE, "(objectClass=*)", ["sAMAccountName"]
        )
        return r[0][1].get("sAMAccountName", [None])[0].decode("utf-8")

    def get_users_recursive(self, dn):
        """
        returns list of user objects in the group, recurses.
        Asks AD to do the recursion, unlike a previous version of this function.
        NB does not throw out non-active users
        """
        res = self.connection.search_s(
            ActiveDirectoryQuery.base_dn,
            ldap.SCOPE_SUBTREE,
            "(&(memberOf:1.2.840.113556.1.4.1941:={0})(!(objectClass=computer))(objectClass=user))".format(  # noqa 501
                dn
            ),
            ["dn"],
        )
        users = [self.lookup_username_from_dn(dn=u[0]) for u in res if u[0]]
        return users


class ActiveDirectorySource(Source):
    def __init__(self, group_name):
        self.group_name = group_name

    def get_members(self):
        ad = ActiveDirectoryQuery()
        users = ad.get_users_recursive(ad.get_group_dn(self.group_name))
        users.sort()
        return users


local_source_factory = LocalSourceFactory()
local_source_factory.register("database", DatabaseSource)
local_source_factory.register("active_directory", ActiveDirectorySource)


class LookupConfig:
    def __init__(
        self, group_name: str, source: Source, commit_comment: str, lookup_options: dict
    ):
        self.group_name = group_name
        self.source = source
        self.commit_comment = commit_comment
        self.lookup_options = lookup_options

    def __repr__(self):
        return (
            f"LookupConfig(group_name={self.group_name!r}, "
            f"source={self.source}, "
            f"commit_comment={self.commit_comment!r})"
        )


class WorkList(abc.ABC):
    @abc.abstractmethod
    def get_work_list(self, config):
        pass


def removeprefix(s, prefix):
    """available as s.removeprefix(prefix) from Python 3.9 onwards"""
    if s.startswith(prefix):
        return s[len(prefix) :]
    else:
        return s


class WorkListFromConfParser(WorkList):
    def get_work_list(self, config):
        prefix = "sync:"
        lookup_sections = [x for x in config.sections() if x.startswith(prefix)]
        work_list = {}
        for section in lookup_sections:
            all_options = dict(config.items(section))
            options = {
                k: v
                for k, v in all_options.items()
                if k not in ["source", "commit_comment", "lookup_options"]
            }
            # while 'database_options' in options:
            #    lookup_from = options['database_options']
            #    del options['database_options']
            #    options.update(dict(config.items(section=lookup_from)))
            if "database_options" in options:
                options["database_options"] = dict(
                    config.items(section=options["database_options"])
                )
            s = local_source_factory.get(
                config.get(section=section, option="source"), **options
            )
            if "lookup_options" in all_options:
                lookup_options = dict(
                    config.items(section=all_options["lookup_options"])
                )
            else:
                lookup_options = {}
            work_list[section] = LookupConfig(
                group_name=removeprefix(section, prefix),
                source=s,
                commit_comment=options.get("commit_comment", None),
                lookup_options=lookup_options,
            )
        return work_list


class LookupGroupMembers:
    def __init__(self, lookup_group_id, lookup_username=None, lookup_password=None):
        self.lookup_group_id = lookup_group_id
        self.lookup_username = lookup_username
        self.lookup_password = lookup_password
        self.conn = None

    def connect(self):
        # print("Connecting to Cambridge Lookup server...")
        # Create an IbisClientConnection object
        self.conn = ibisclient.createConnection()
        # Create an GroupMethods object
        self.group_methods = ibisclient.GroupMethods(self.conn)
        if self.lookup_username and self.lookup_password:
            # Authenticate for write access
            self.conn.set_username(self.lookup_username)
            self.conn.set_password(self.lookup_password)

    def get_members(self):
        """Returns list of all member CRSIDs in a lookup group"""
        if self.conn is None:
            self.connect()
        # print("Getting list of people in Chemistry group...")
        lookup_people = self.group_methods.getMembers(self.lookup_group_id)
        return [person.identifier.value for person in lookup_people]

    def set_members(
        self, intended_members, verbose=False, commit_comment=None, dry_run=False
    ):
        # this will create a connection to lookup if we haven't got one already
        l_list = self.get_members()
        # Create list of CRSIDs intended but missing in lookup
        missing_list = list(set(intended_members) - set(l_list))
        missing_list.sort()
        # Create list of CRSIDs in lookup group but not in the intended set
        remove_list = list(set(l_list) - set(intended_members))
        remove_list.sort()

        if verbose:
            if missing_list:
                # Print list of missing people
                print(
                    "{count} members missing from lookup group:".format(
                        count=len(missing_list)
                    )
                )
                print(" ".join(str(x) for x in missing_list))
            else:
                # No people missing from lookup group
                print("No members to be added to lookup group")
            if remove_list:
                # Print list of people to remove
                print(
                    "{count} members to be removed from lookup group:".format(
                        count=len(remove_list)
                    )
                )
                print(" ".join(str(x) for x in remove_list))
            else:
                # No people to removed from lookup group
                print("No members to be removed from lookup group.")

        # Build comma delimited list for adding to group
        to_add = str(",".join(str(x) for x in missing_list))
        # Build comma delimited list for removal
        to_rem = str(",".join(str(x) for x in remove_list))

        if dry_run:
            return {"to_add": to_add, "to_remove": to_rem}

        if missing_list or remove_list:
            # Update lookup group
            if verbose:
                print("Updating lookup group members...")

            self.group_methods.updateDirectMembers(
                self.lookup_group_id, to_add, to_rem, commitComment=commit_comment
            )
            # check whether the updates all worked or not
            if verbose:
                print("Verifying changes...")
            l_list_pu = self.get_members()
            failed = list(set(missing_list) - set(l_list_pu))
            failed.sort()
            succeeded_adding = list(set(missing_list) - set(failed))
            succeeded_adding.sort()
            succeeded_removing = list(set(l_list) - set(l_list_pu))
            succeeded_removing.sort()
            if verbose:
                # If people added successfully
                if succeeded_adding:
                    # Report successes
                    print(
                        "The following {count} people were added successfully:".format(
                            count=len(succeeded_adding)
                        )
                    )
                    print(" ".join(str(x) for x in succeeded_adding))
                else:
                    # Report no one added
                    print("No one was added to the group.")

                # If people removed successfully
                if succeeded_removing:
                    # Report removed
                    print(
                        "The following {count} people were removed successfully:".format(
                            count=len(succeeded_removing)
                        )
                    )
                    print(" ".join(str(x) for x in succeeded_removing))
                else:
                    # Report no one removed
                    print("No one was removed from the group.")

                # If there were failures
                if failed:
                    # Report failures
                    print(
                        "The following {count} people could not be added (inactive CRSid):".format(
                            count=len(failed)
                        )
                    )
                    print(" ".join(str(x) for x in failed))
            return {
                "added": succeeded_adding,
                "removed": succeeded_removing,
                "failed_add": failed,
            }
        if verbose:
            print(
                (
                    "Group already matches intended list. No changes need to be made. "
                    "Contains {count} members.".format(count=len(l_list))
                )
            )
        # no changes needed to be made
        return None


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument("--dry-run", action="store_true", default=False)
    parser.add_argument("--verbose", action="store_true", default=False)
    parser.add_argument(
        "config_file", type=str, help="e.g. /etc/lookup-sync/lookup-sync.cfg"
    )
    args = parser.parse_args()

    # Create config parser object
    config = configparser.ConfigParser()

    # Read config file
    config.read(args.config_file)
    if not config.sections():
        raise Exception(
            f"Cannot read the configuration file {args.config_file!r} or it is empty!"
        )

    w = WorkListFromConfParser()
    work_list = w.get_work_list(config)
    for key, work_item in work_list.items():
        if args.verbose:
            print(key)
        lookup = LookupGroupMembers(
            work_item.group_name,
            **{f"lookup_{k}": v for k, v in work_item.lookup_options.items()},
        )
        ret = lookup.set_members(
            intended_members=work_item.source.get_members(),
            commit_comment=work_item.commit_comment,
            dry_run=args.dry_run,
            verbose=args.verbose,
        )
        if args.verbose:
            # pprint.pprint(ret)  # same info already printed within the set_members call
            print("---")
